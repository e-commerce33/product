'use strict';

const log = require('../helpers/log'),
  mongoose = require('../loaders/mongoose');

const productsData = require('../data/initData.json');

let Model = null;

function exec() {
  mongoose.init()
    .then(() => {
      Model = require('../controllers/products/model');
      return Model.countDocuments();
    })
    .then((count) => {
      if (count === 0) {
        log.debug('Init product data');
        return Model.create(productsData);
      }

      log.debug('This service have already data');

      return null;
    })
    .finally(mongoose.disconnect);
}

module.exports = { exec };
