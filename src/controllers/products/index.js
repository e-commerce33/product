/* eslint-disable no-underscore-dangle */
'use strict';

const Model = require('./model'),
  _ = require('lodash'),
  apiSchema = require('./apiSchema'),
  httpStatus = require('../../constants/httpStatus'),
  log = require('../../helpers/log');

function createSearchCondition(query) {
  const condition = _.cloneDeep(query);
  if (condition._id) {
    condition._id = { $in: condition._id };
  }
  if (condition.name) {
    condition.name = { $regex: new RegExp(`.*${condition.name}.*`, 'iu') };
  }
  return condition;
}

function search(req, res, next) {
  log.debug('products.search');

  return apiSchema.search.query.validateAsync(req.query)
    .then((value) => {
      const query = createSearchCondition(value);
      return Model.find(query);
    })
    .then((result) => res.status(httpStatus.OK).json(result))
    .catch(next);
}

module.exports = { search };
