'use strict';

const joi = require('joi');

const schema = {
  _id: joi.string().hex().length(24),
  name: joi.string().min(1).max(50).trim()
};

module.exports = {
  search: {
    query: joi.object().keys({
      // eslint-disable-next-line no-underscore-dangle
      _id: joi.array().items(schema._id).single().min(1)
        .optional(),
      name: schema.name.optional()
    })
  }
};
