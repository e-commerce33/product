'use strict';

const mongoose = require('mongoose');

const mainDb = require('../../loaders/mongoose').getConnection();

const Schema = mongoose.Schema;

const productSchena = new Schema({
  name: {
    type: String,
    required: true
  },
  price: {
    type: Number,
    min: 0,
    required: true
  },
  stock: {
    type: Number,
    min: 0,
    required: true
  },
  description: {
    type: String,
    default: null
  }
}, { timestamps: true });

module.exports = mainDb.model('Product', productSchena, 'products');
