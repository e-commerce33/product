'use strict';

const { combine } = require('../helpers/api'),
  products = require('./products');

const combined = combine({
  products
});

module.exports = combined;
