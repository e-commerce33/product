'use strict';

const isAuthorizedAccess = require('./isAuthorizedAccess'),
  isAuthorizedAccessService = require('./isAuthorizedAccessService');

module.exports = { isAuthorizedAccess, isAuthorizedAccessService };
