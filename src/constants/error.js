'use strict';

const _ = require('lodash');

const httpStatus = require('./httpStatus');

const errorObjects = {
  BAD_REQUEST: {
    statusCode: httpStatus.BAD_REQUEST,
    message: 'invalid parameter(s)'
  },
  UNAUTHORIZED: {
    statusCode: httpStatus.UNAUTHORIZED,
    message: 'unauthorized'
  },
  FORBIDDEN: {
    statusCode: httpStatus.FORBIDDEN,
    message: 'forbidden'
  },
  NOT_FOUND: {
    statusCode: httpStatus.NOT_FOUND,
    message: 'resource not found'
  },
  CANNOT_CREATE: {
    statusCode: httpStatus.CANNOT_CREATE,
    message: 'cannot create resource'
  },
  CONFLICT: {
    statusCode: httpStatus.CONFLICT,
    message: 'conflict'
  },
  INTERNAL_SERVER_ERROR: {
    statusCode: httpStatus.INTERNAL_SERVER_ERROR,
    message: 'somethings went wrong'
  }
};

// add isCustom flag to error objects
_.forEach(errorObjects, (obj) => {
  obj.isCustom = true;
});

module.exports = { errorObjects };
